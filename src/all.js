const {HttpMethods} = global.SnappLab;

return [
  HttpMethods.get,
  HttpMethods.post,
  HttpMethods.put,
  HttpMethods.delete
];
